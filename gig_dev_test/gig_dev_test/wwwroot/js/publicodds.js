﻿"use strict";

var oddsDisplay = function (data) {
    $("#odds").html('<div class="well"><b>Refreshed: ' + Date(Date.now()).toString() + "</b></div>");
    data.forEach(function (element) {
        var odd = '<div class="panel panel-default">';
        odd += '<div class="panel-heading"><h3 class="panel-title">' + element.homeTeamName + " vs " + element.guestTeamName + '</h3></div>';
        odd += '<div class="panel-body">';
        odd += '<div> Odds1: <span class="badge">' + element.odd1 + "</span></div>";
        odd += '<div> OddsX: <span class="badge">' + element.oddX + "</span></div>";
        odd += '<div> Odds2: <span class="badge">' + element.odd2 + "</span></div>";
        odd += '</div></div>';
        $("#odds").append(odd);
    });
};

$(document).ready(function () {

    var options = {};
    options.url = "/api/publicodds";
    options.type = "GET";
    options.dataType = "json";
    options.success = oddsDisplay;
    options.error = function () {
        $("#msg").html("Error while calling the Web API!");
    };
    $.ajax(options);
});



var connection = new signalR.HubConnectionBuilder().withUrl("/oddshub").build();
connection.start();

connection.on("OddsPublished", function (lst) {
    oddsDisplay(lst);
});
