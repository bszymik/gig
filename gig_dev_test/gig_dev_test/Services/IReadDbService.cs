﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Services
{
    public interface IReadDbService<T>
    {
        Task<List<T>> GetAll();
        Task<T> GetSingle(int id);
    }
}
