﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Services
{
    public interface IDbService<T> : IReadDbService<T>
    {
        
        Task Delete(int id);
        Task Edit(T t);
        Task Create(T t);
        Task Publish();
    }
}
