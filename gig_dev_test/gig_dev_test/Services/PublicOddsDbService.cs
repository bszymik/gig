﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gig_dev_test.Data;
using gig_dev_test.Models.DTO;
using gig_dev_test.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace gig_dev_test.Services
{
    /// <summary>
    /// Service for getting the public odds
    /// </summary>
    public class PublicOddsDbService : IReadDbService<PublicOddsDTO>
    {
        private readonly ApplicationDbContext _db;
        public PublicOddsDbService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<PublicOddsDTO>> GetAll()
        {
            return await _db.PublicOdds.Include(o => o.GuestTeam)
                .Include(o => o.HomeTeam).Select(po => DTOFactory.Create(po)).ToListAsync();
        }

        public async Task<PublicOddsDTO> GetSingle(int id)
        {
            var pOdd = await _db.PublicOdds.Include(o => o.GuestTeam).Include(o => o.HomeTeam).FirstOrDefaultAsync(o => o.Id == id);
            return DTOFactory.Create(pOdd);
        }
    }
}
