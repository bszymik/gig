﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gig_dev_test.Data;
using gig_dev_test.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace gig_dev_test.Services
{
    public class OddsDbService : IDbService<Odd>
    {
        private readonly ApplicationDbContext _db;
        public OddsDbService(ApplicationDbContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Get all Odds
        /// </summary>
        /// <returns></returns>
        public async Task<List<Odd>> GetAll()
        {
            return await _db.Odds.Include(o => o.GuestTeam).Include(ot => ot.HomeTeam).ToListAsync();
        }

        /// <summary>
        /// Get single Odd item by it's ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Odd> GetSingle(int id)
        {
            return await _db.Odds.Include(o => o.GuestTeam).Include(o => o.HomeTeam).FirstOrDefaultAsync(o => o.Id == id);
        }

        /// <summary>
        /// Delete Odd by id
        /// Remark: return should stay Task not void - void will cause dbContext Dispose
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            var odd = _db.Odds.FirstOrDefault(o => o.Id == id);
            _db.Odds.Remove(odd);

            await _db.SaveChangesAsync();
        }

        public async Task Edit(Odd odd)
        {
            _db.Odds.Update(odd);
            await _db.SaveChangesAsync();
        }

        public async Task Create(Odd odd)
        {
            _db.Odds.Add(odd);
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// move copy odds from Odds table to PublicOdds table
        /// </summary>
        /// <returns></returns>
        public async Task Publish()
        {
            _db.PublicOdds.RemoveRange(_db.PublicOdds);
            await _db.SaveChangesAsync();
            
            _db.PublicOdds.AddRange(_db.Odds.Select(o => ModelFactory.CreatePublicOdd(o)));
            await _db.SaveChangesAsync();

        }
    }
}
