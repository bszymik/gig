﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gig_dev_test.Data;
using Microsoft.EntityFrameworkCore;

namespace gig_dev_test.Services
{
    public class TeamsDbService : IReadDbService<Team>
    {
        private readonly ApplicationDbContext _db;
        public TeamsDbService(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<List<Team>> GetAll()
        {
            return await _db.Teams.ToListAsync();
        }

        public async Task<Team> GetSingle(int id)
        {
            return await _db.Teams.FirstOrDefaultAsync(o => o.Id == id);
        }
        
    }
}
