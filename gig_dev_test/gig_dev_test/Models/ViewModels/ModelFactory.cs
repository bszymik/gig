﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gig_dev_test.Data;

namespace gig_dev_test.Models.ViewModels
{
    /// <summary>
    /// factory for Models conversion
    /// </summary>
    public class ModelFactory
    {
        /// <summary>
        /// Converts Odd into OddViewModel
        /// </summary>
        /// <param name="odd"></param>
        /// <returns></returns>
        public static OddViewModel Create(Odd odd)
        {
            return new OddViewModel()
            {
                Id = odd.Id,
                HomeTeamName = odd.HomeTeam.TeamName,
                GuestTeamName = odd.GuestTeam.TeamName,
                Odd1 = odd.Odd1,
                Odd2 = odd.Odd2,
                OddX = odd.OddX
            };
        }
        
        /// <summary>
        /// Converts List of Odds into List of OddViewModels
        /// </summary>
        /// <param name="odds"></param>
        /// <returns></returns>
        public static IList<OddViewModel> Create(IList<Odd> odds)
        {
            return odds.Select(o => Create(o)).ToList();
        }

        /// <summary>
        /// convert from single Odd to public Odds
        /// </summary>
        /// <param name="odd"></param>
        /// <returns></returns>
        public static PublicOdd CreatePublicOdd(Odd odd)
        {
            return new PublicOdd()
            {
                HomeTeamId = odd.HomeTeamId,
                GuestTeamId = odd.GuestTeamId,
                Odd1 = odd.Odd1,
                Odd2 = odd.Odd2,
                OddX = odd.OddX
            };
        }

        public static OddViewModel CreatePublicOdd(PublicOdd odd)
        {
            return new OddViewModel()
            {
                Id = odd.Id,
                HomeTeamName = odd.HomeTeam.TeamName,
                GuestTeamName = odd.GuestTeam.TeamName,
                Odd1 = odd.Odd1,
                Odd2 = odd.Odd2,
                OddX = odd.OddX
            };
        }
    }
}
