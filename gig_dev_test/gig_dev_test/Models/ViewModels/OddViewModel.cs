﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Models.ViewModels
{
    /// <summary>
    /// ViewModel representing Odds with TeamNames already included
    /// </summary>
    public class OddViewModel
    {
        public int Id { get; set; }
        public string HomeTeamName { get; set; }
        
        //public int HomeTeamId { get; set; }

        public string GuestTeamName { get; set; }
        
        //public int GuestTeamId { get; set; }

        public double Odd1 { get; set; }
        public double OddX { get; set; }
        public double Odd2 { get; set; }
    }
}
