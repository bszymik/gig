﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Models.DTO
{
    /// <summary>
    /// Data transfer object for Public Odds
    /// </summary>
    public class PublicOddsDTO
    {
        public string HomeTeamName { get; set; }
        
        public string GuestTeamName { get; set; }
        
        public double Odd1 { get; set; }
        public double OddX { get; set; }
        public double Odd2 { get; set; }
    }
}
