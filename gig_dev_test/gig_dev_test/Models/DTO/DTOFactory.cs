﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gig_dev_test.Data;
using gig_dev_test.Models.ViewModels;

namespace gig_dev_test.Models.DTO
{
    public class DTOFactory
    {
        public static PublicOddsDTO Create(PublicOdd odd)
        {
            return new PublicOddsDTO()
            {
                HomeTeamName = odd.HomeTeam.TeamName,
                GuestTeamName = odd.GuestTeam.TeamName,
                Odd1 = odd.Odd1,
                Odd2 = odd.Odd2,
                OddX = odd.OddX
            };
        }
    }
}
