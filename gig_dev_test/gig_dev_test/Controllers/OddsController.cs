﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using gig_dev_test.Data;
using gig_dev_test.Hubs;
using gig_dev_test.Models.DTO;
using gig_dev_test.Models.ViewModels;
using gig_dev_test.Services;
using Microsoft.AspNetCore.SignalR;

namespace gig_dev_test.Controllers
{
    /// <summary>
    /// Odds controller for managing odds
    /// </summary>
    public class OddsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<OddsHub> _hubcontext;
        private readonly IDbService<Odd> _oddsService;
        private readonly IReadDbService<Team> _teamsService;
        private readonly IReadDbService<PublicOddsDTO> _pubOdds;
        public OddsController(ApplicationDbContext context, IHubContext<OddsHub> hubcontext = null)
        {
            _context = context;
            _oddsService = new OddsDbService(_context);
            _teamsService = new TeamsDbService(_context);
            _pubOdds = new PublicOddsDbService(_context);
            _hubcontext = hubcontext;
        }

        // GET: Odds
        public async Task<IActionResult> Index()
        {
            var allOdds = await _oddsService.GetAll();
            var allOddsVm = ModelFactory.Create(allOdds);
            return View(allOddsVm);
        }

        // GET: Odds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var odd = await _oddsService.GetSingle(id.Value);

            if (odd == null)
            {
                return NotFound();
            }

            return View(ModelFactory.Create(odd));
        }

        // POST: Odds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _oddsService.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        // GET: Odds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var odd = await _oddsService.GetSingle(id.Value);
            if (odd == null)
            {
                return NotFound();
            }
            var teams = await _teamsService.GetAll();
            ViewData["GuestTeamId"] = new SelectList(teams, "Id", "TeamName", odd.GuestTeamId);
            ViewData["HomeTeamId"] = new SelectList(teams, "Id", "TeamName", odd.HomeTeamId);
            return View(odd);
        }

        // POST: Odds/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,HomeTeamId,GuestTeamId,Odd1,OddX,Odd2")] Odd odd)
        {
            if (id != odd.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _oddsService.Edit(odd);
                return RedirectToAction(nameof(Index));
            }
            var teams = await _teamsService.GetAll();
            ViewData["GuestTeamId"] = new SelectList(teams, "Id", "TeamName", odd.GuestTeamId);
            ViewData["HomeTeamId"] = new SelectList(teams, "Id", "TeamName", odd.HomeTeamId);
            return View(odd);
        }

        // GET: Odds/Create
        public async Task<IActionResult> Create()
        {
            var teams = await _teamsService.GetAll();
            ViewData["GuestTeamId"] = new SelectList(teams, "Id", "TeamName");
            ViewData["HomeTeamId"] = new SelectList(teams, "Id", "TeamName");
            return View();
        }

        // POST: Odds/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,HomeTeamId,GuestTeamId,Odd1,OddX,Odd2")] Odd odd)
        {
            if (ModelState.IsValid)
            {
                await _oddsService.Create(odd);
                return RedirectToAction(nameof(Index));
            }
            var teams = await _teamsService.GetAll();
            ViewData["GuestTeamId"] = new SelectList(teams, "Id", "TeamName", odd.GuestTeamId);
            ViewData["HomeTeamId"] = new SelectList(teams, "Id", "TeamName", odd.HomeTeamId);
            return View(odd);
        }

        // GET: Odds/Create
        public async Task<IActionResult> Publish()
        {
            await _oddsService.Publish();
            var pubOdds = await _pubOdds.GetAll();
            if (_hubcontext != null)
                await _hubcontext.Clients.All.SendAsync("OddsPublished", pubOdds);

            return RedirectToAction("Index", "Home");
        }
    }
}
