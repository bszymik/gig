﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gig_dev_test.Data;
using gig_dev_test.Models.DTO;
using gig_dev_test.Services;

namespace gig_dev_test.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublicOddsController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IReadDbService<PublicOddsDTO> _publicOdds;
        public PublicOddsController(ApplicationDbContext context)
        {
            _db = context;
            _publicOdds = new PublicOddsDbService(_db);
        }

        // GET: api/PublicOdds
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicOddsDTO>>> GetPublicOdds()
        {
            return await _publicOdds.GetAll();
        }
        
    }
}
