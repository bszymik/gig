﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace gig_dev_test.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Odd> Odds { get; set; }

        public virtual DbSet<PublicOdd> PublicOdds { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// parameterless constructor for Moq
        /// </summary>
        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            //Seed the teams and odds
            modelBuilder.Entity<Team>().HasData(
                new Team { Id = 1, TeamName = "EMIRATES TEAM NEW ZEALAND" },
                new Team { Id = 2, TeamName = "MALTA ALTUS CHALLENGE" },
                new Team { Id = 3, TeamName = "LUNA ROSSA CHALLENGE" },
                new Team { Id = 4, TeamName = "NEW YORK YACHT CLUB AMERICAN MAGIC" },
                new Team { Id = 5, TeamName = "INEOS TEAM UK" },
                new Team { Id = 6, TeamName = "STARS & STRIPES TEAM USA" },
                new Team { Id = 7, TeamName = "DUTCHSAIL AC36" }
            );
            
            modelBuilder.Entity<Odd>().HasData(
                new Odd
                {
                    Id = 1,
                    HomeTeamId = 1,
                    GuestTeamId = 2,
                    Odd1 = 0.1,
                    OddX = 1,
                    Odd2 = 5
                },
                new Odd
                {
                    Id = 2,
                    HomeTeamId = 2,
                    GuestTeamId = 1,
                    Odd1 = 0.1,
                    OddX = 1,
                    Odd2 = 5
                }
            );

            modelBuilder.Entity<PublicOdd>().HasData(
                new PublicOdd
                {
                    Id = 1,
                    HomeTeamId = 1,
                    GuestTeamId = 2,
                    Odd1 = 0.1,
                    OddX = 1,
                    Odd2 = 5
                });
        }
        
    }
}
