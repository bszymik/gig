﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Data
{
    /// <summary>
    /// represents Odd with Teams and 1X2 odds values
    /// </summary>
    public class Odd
    {
        public int Id { get; set; }

        public Team HomeTeam { get; set; }
        [ForeignKey("HomeTeam")]
        public int HomeTeamId { get; set; }

        public Team GuestTeam { get; set; }
        [ForeignKey("GuestTeam")]
        public int GuestTeamId { get; set; }

        public double Odd1 { get; set; }
        public double OddX { get; set; }
        public double Odd2 { get; set; }
    }
}
