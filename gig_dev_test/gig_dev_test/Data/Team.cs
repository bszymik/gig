﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gig_dev_test.Data
{
    /// <summary>
    /// class representing team
    /// </summary>
    public class Team
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
    }
}
