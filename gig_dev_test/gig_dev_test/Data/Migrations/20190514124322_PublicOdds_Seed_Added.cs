﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gig_dev_test.Data.Migrations
{
    public partial class PublicOdds_Seed_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "PublicOdds",
                columns: new[] { "Id", "GuestTeamId", "HomeTeamId", "Odd1", "Odd2", "OddX" },
                values: new object[] { 1, 2, 1, 0.10000000000000001, 5.0, 1.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PublicOdds",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
