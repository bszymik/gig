﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gig_dev_test.Data.Migrations
{
    public partial class Seed_Teams_Odds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "TeamName" },
                values: new object[,]
                {
                    { 1, "EMIRATES TEAM NEW ZEALAND" },
                    { 2, "MALTA ALTUS CHALLENGE" },
                    { 3, "LUNA ROSSA CHALLENGE" },
                    { 4, "NEW YORK YACHT CLUB AMERICAN MAGIC" },
                    { 5, "INEOS TEAM UK" },
                    { 6, "STARS & STRIPES TEAM USA" },
                    { 7, "DUTCHSAIL AC36" }
                });

            migrationBuilder.InsertData(
                table: "Odds",
                columns: new[] { "Id", "GuestTeamId", "HomeTeamId", "Odd1", "Odd2", "OddX" },
                values: new object[] { 1, 2, 1, 0.10000000000000001, 5.0, 1.0 });

            migrationBuilder.InsertData(
                table: "Odds",
                columns: new[] { "Id", "GuestTeamId", "HomeTeamId", "Odd1", "Odd2", "OddX" },
                values: new object[] { 2, 1, 2, 0.10000000000000001, 5.0, 1.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Odds",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Odds",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
