﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace gig_dev_test.Data.Migrations
{
    public partial class PublicOdds_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PublicOdds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HomeTeamId = table.Column<int>(nullable: false),
                    GuestTeamId = table.Column<int>(nullable: false),
                    Odd1 = table.Column<double>(nullable: false),
                    OddX = table.Column<double>(nullable: false),
                    Odd2 = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicOdds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PublicOdds_Teams_GuestTeamId",
                        column: x => x.GuestTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PublicOdds_Teams_HomeTeamId",
                        column: x => x.HomeTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PublicOdds_GuestTeamId",
                table: "PublicOdds",
                column: "GuestTeamId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicOdds_HomeTeamId",
                table: "PublicOdds",
                column: "HomeTeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PublicOdds");
        }
    }
}
