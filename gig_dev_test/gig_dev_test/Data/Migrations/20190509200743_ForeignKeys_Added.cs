﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gig_dev_test.Data.Migrations
{
    public partial class ForeignKeys_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Odds_Teams_GuestTeamId",
                table: "Odds");

            migrationBuilder.DropForeignKey(
                name: "FK_Odds_Teams_HomeTeamId",
                table: "Odds");

            migrationBuilder.AlterColumn<int>(
                name: "HomeTeamId",
                table: "Odds",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GuestTeamId",
                table: "Odds",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Odds_Teams_GuestTeamId",
                table: "Odds",
                column: "GuestTeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Odds_Teams_HomeTeamId",
                table: "Odds",
                column: "HomeTeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Odds_Teams_GuestTeamId",
                table: "Odds");

            migrationBuilder.DropForeignKey(
                name: "FK_Odds_Teams_HomeTeamId",
                table: "Odds");

            migrationBuilder.AlterColumn<int>(
                name: "HomeTeamId",
                table: "Odds",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GuestTeamId",
                table: "Odds",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Odds_Teams_GuestTeamId",
                table: "Odds",
                column: "GuestTeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Odds_Teams_HomeTeamId",
                table: "Odds",
                column: "HomeTeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
