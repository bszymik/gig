﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using gig_dev_test.Controllers;
using gig_dev_test.Controllers.api;
using gig_dev_test.Data;
using gig_dev_test.Models.DTO;
using gig_dev_test.Models.ViewModels;
using gig_dev_test.xUnit.DbContextMock;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace gig_dev_test.xUnit.ControllerTests
{
    public class ApiController_test
    {
        [Fact]
        public async Task PublicOdds_Get_Test()
        {
            //Arrange
            var appDbContext = MockDbContextHelper.InMemoryDbContext();
            var pubOddsCtrl = new PublicOddsController(appDbContext);

            //Act
            var lst = await pubOddsCtrl.GetPublicOdds();

            //Assert
            Assert.NotNull(lst);


            var indexResult = Assert.IsType<ActionResult<IEnumerable<PublicOddsDTO>>>(lst);
            
        }
    }
}
