﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using gig_dev_test.Controllers;
using gig_dev_test.Data;
using gig_dev_test.Models.ViewModels;
using gig_dev_test.xUnit.DbContextMock;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace gig_dev_test.xUnit.MvcControllerTests
{
    public class OddsController_Test
    {
        [Fact]
        public async Task Odds_Index_Return_ListOfOdds_Test()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);
            
            //Act
            var index = await oddsCtrl.Index();

            //Assert
            Assert.IsType<ViewResult>(index);

            var indexResult = Assert.IsType<ViewResult>(index);
            var model = Assert.IsAssignableFrom<IEnumerable<OddViewModel>>(
                indexResult.ViewData.Model);
            Assert.NotEmpty(model);

        }

        [Fact]
        public async Task Odds_DeleteCOnfirmation_ShowsOdd()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var delete = await oddsCtrl.Delete(1);

            //Assert
            Assert.IsType<ViewResult>(delete);

            var deleteResult = Assert.IsType<ViewResult>(delete);
            var model = Assert.IsAssignableFrom<OddViewModel>(
                deleteResult.ViewData.Model);
            Assert.NotNull(model);

        }

        [Fact]
        public async Task Odds_DeleteCOnfirmation_Null_Id_Return_NotFound()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var delete = await oddsCtrl.Delete(null);

            //Assert
            Assert.IsType<NotFoundResult>(delete);
            
        }

        [Fact]
        public async Task Odds_DeleteCOnfirmation_NonExisting_Id_Return_NotFound()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var delete = await oddsCtrl.Delete(111);

            //Assert
            Assert.IsType<NotFoundResult>(delete);

        }

        [Fact]
        public async Task Odds_DeleteConfirmed()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var delete = await oddsCtrl.DeleteConfirmed(1);

            //Assert
            Assert.IsType<RedirectToActionResult>(delete);

        }


        [Fact]
        public async Task Odds_EditScreen_ShowsOdd()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var edit = await oddsCtrl.Edit(1);

            //Assert
            Assert.IsType<ViewResult>(edit);

            var editResult = Assert.IsType<ViewResult>(edit);
            var model = Assert.IsAssignableFrom<Odd>(
                editResult.ViewData.Model);
            Assert.NotNull(model);

        }

        [Fact]
        public async Task Odds_EditScreen_Null_Id_Return_NotFound()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var edit = await oddsCtrl.Edit(null);

            //Assert
            Assert.IsType<NotFoundResult>(edit);

        }

        [Fact]
        public async Task Odds_EditScreen_NonExisting_Id_Return_NotFound()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var edit = await oddsCtrl.Edit(111);

            //Assert
            Assert.IsType<NotFoundResult>(edit);
        }

        [Fact]
        public async Task Odds_EditScreen_EditItem_ModelStateInvalid()
        {
            //Arrange
            var appDbContext = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContext.Object);
            oddsCtrl.ModelState.AddModelError("test", "test");

            //Act
            var create = await oddsCtrl.Edit(5, new Odd() {Id = 5});

            //Assert

            var indexResult = Assert.IsType<ViewResult>(create);
            var model = Assert.IsAssignableFrom<Odd>(
                indexResult.ViewData.Model);
            Assert.NotNull(model);
        }

        [Fact]
        public async Task Odds_EditScreen_EditItem_NotFound()
        {
            //Arrange
            var appDbContext = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContext.Object);
            
            //Act
            var create = await oddsCtrl.Edit(5, new Odd() { Id = 6 });

            //Assert

            var indexResult = Assert.IsType<NotFoundResult>(create);
            
        }

        [Fact]
        public async Task Odds_CreateScreen_ReturnsVeiw()
        {
            //Arrange
            var appDbContextMock = MockDbContextHelper.SampleDbContext();
            var oddsCtrl = new OddsController(appDbContextMock.Object);

            //Act
            var create = await oddsCtrl.Create();

            //Assert
            Assert.IsType<ViewResult>(create);
        }

        [Fact]
        public async Task Odds_EditScreen_EditItem_Edited_RedirectTOAction()
        {
            //Arrange
            var appDbContextInMemory = MockDbContextHelper.InMemoryDbContext();
            var oddsCtrl = new OddsController(appDbContextInMemory);
            var oddToEdit = appDbContextInMemory.Odds.FirstOrDefault();

            //Act
            oddToEdit.Odd1 = 8.0;
            var edit = await oddsCtrl.Edit(oddToEdit.Id, oddToEdit);

            //Assert

            var indexResult = Assert.IsType<RedirectToActionResult>(edit);
        }

        [Fact]
        public async Task Odds_CreateScreen_CreateItem_RedirecToAction()
        {
            //Arrange
            var appDbContextInMemory = MockDbContextHelper.InMemoryDbContext();
            var oddsCtrl = new OddsController(appDbContextInMemory);

            //Act
            var create = await oddsCtrl.Create(new Odd()
            {
                Id = 5,
                GuestTeamId = 1,
                HomeTeamId = 2,
                Odd1 = 1.0,
                Odd2 = 2.1,
                OddX = 3.3
            });
            
            //Assert
            Assert.IsType<RedirectToActionResult>(create);
        }

        [Fact]
        public async Task Odds_CreateScreen_CreateItem_ModelStateInvalid()
        {
            //Arrange
            var appDbContextInMemory = MockDbContextHelper.InMemoryDbContext();
            var oddsCtrl = new OddsController(appDbContextInMemory);
            oddsCtrl.ModelState.AddModelError("test", "test");

            //Act
            var create = await oddsCtrl.Create(new Odd());

            //Assert
            
            var indexResult = Assert.IsType<ViewResult>(create);
            var model = Assert.IsAssignableFrom<Odd>(
                indexResult.ViewData.Model);
            Assert.NotNull(model);
        }

        [Fact]
        public async Task Odds_Publish_RedirecToAction()
        {
            //Arrange
            var appDbContextInMemory = MockDbContextHelper.InMemoryDbContext();
            var oddsCtrl = new OddsController(appDbContextInMemory);

            //Act
            var publish = await oddsCtrl.Publish();

            //Assert
            Assert.IsType<RedirectToActionResult>(publish);
        }
    }
}
