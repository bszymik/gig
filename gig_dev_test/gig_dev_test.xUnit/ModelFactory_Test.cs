﻿using System;
using System.Collections.Generic;
using System.Text;
using gig_dev_test.Data;
using gig_dev_test.Models.DTO;
using gig_dev_test.Models.ViewModels;
using Xunit;

namespace gig_dev_test.xUnit
{
    /// <summary>
    /// test proper creation of ViewModels
    /// </summary>
    public class ModelFactory_Test
    {
        [Fact]
        public void OddViewModel_FactoryTest()
        {
            //Arrange
            Odd odd = new Odd()
            {
                GuestTeam = new Team()
                {
                    TeamName = "test guest name"
                },
                HomeTeam = new Team()
                {
                    TeamName = "test home name"
                },
                Odd1 = 1.0,
                Odd2 = 2.0,
                OddX = 5.0
            };
            //Act

            var oddVM = ModelFactory.Create(odd);

            //Assert
            Assert.Equal(odd.Id, oddVM.Id);

            Assert.Equal(odd.GuestTeam.TeamName, oddVM.GuestTeamName);
            Assert.Equal(odd.HomeTeam.TeamName, oddVM.HomeTeamName);

            Assert.Equal(odd.Odd1, oddVM.Odd1);
            Assert.Equal(odd.Odd2, oddVM.Odd2);
            Assert.Equal(odd.OddX, oddVM.OddX);

        }

        [Fact]
        public void PublicOdds_DTO_FactoryTest()
        {
            //Arrange
            PublicOdd odd = new PublicOdd()
            {
                Id = 1,
                GuestTeam = new Team()
                {
                    TeamName = "test guest name"
                },
                HomeTeam = new Team()
                {
                    TeamName = "test home name"
                },
                Odd1 = 1.0,
                Odd2 = 2.0,
                OddX = 5.0
            };
            //Act

            var oddDTO = DTOFactory.Create(odd);

            //Assert
            
            Assert.Equal(odd.GuestTeam.TeamName, oddDTO.GuestTeamName);
            Assert.Equal(odd.HomeTeam.TeamName, oddDTO.HomeTeamName);

            Assert.Equal(odd.Odd1, oddDTO.Odd1);
            Assert.Equal(odd.Odd2, oddDTO.Odd2);
            Assert.Equal(odd.OddX, oddDTO.OddX);

        }
    }
}
