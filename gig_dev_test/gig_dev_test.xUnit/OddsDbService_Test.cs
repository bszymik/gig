using System.Linq;
using gig_dev_test.Services;
using System.Threading;
using System.Threading.Tasks;
using gig_dev_test.Data;
using gig_dev_test.Models.DTO;
using gig_dev_test.xUnit.DbContextMock;
using Moq;
using Xunit;

namespace gig_dev_test.xUnit
{
    public class OddsDbService_Test
    {
        private readonly Mock<ApplicationDbContext> appDbContext;
        public OddsDbService_Test()
        {
            appDbContext = MockDbContextHelper.SampleDbContext();
        }
        [Fact]
        public async Task GetAll_ItemsAsync()
        {
            //Arrange
            IDbService<Odd> odsService = new OddsDbService(MockDbContextHelper.InMemoryDbContext()); //appDbContext.Object);
            //Act
            var allodds = await odsService.GetAll();

            //Assert
            Assert.NotEmpty(allodds);
        }

        [Fact]
        public async Task GetSingle_Item()
        {
            //Arrange
            IDbService<Odd> odsService = new OddsDbService(MockDbContextHelper.InMemoryDbContext());
            //Act
            var odd = await odsService.GetSingle(1);

            //Assert
            Assert.NotNull(odd);
            Assert.Equal(1, odd.Id);
        }

        [Fact]
        public async Task Delete_ItemMethodCall()
        {
            //Arrange
            var ctx = appDbContext.Object;
            IDbService<Odd> odsService = new OddsDbService(ctx);
            //Act
            await odsService.Delete(1);
            
            //Assert
            appDbContext.Verify(c => c.SaveChangesAsync(new CancellationToken()), Times.Once);
        }

        [Fact]
        public async Task Delete_Item_OneItemLess()
        {
            //Arrange
            var ctx = MockDbContextHelper.InMemoryDbContext();
            IDbService<Odd> odsService = new OddsDbService(ctx);
            int oddsCount = ctx.Odds.Count();
            //Act
            await odsService.Delete(1);

            //Assert
            Assert.Equal(oddsCount-1, ctx.Odds.Count());
        }

        [Fact]
        public async Task Edit_Item_MethodCall()
        {
            //Arrange
            var ctx = appDbContext.Object;
            IDbService<Odd> odsService = new OddsDbService(ctx);
            //Act

            var odd = await odsService.GetSingle(1);
            odd.Odd1 = 3;
            odd.Odd2 = 3;
            odd.OddX = 3;

            await odsService.Edit(odd);
            
            //Assert
            appDbContext.Verify(c => c.SaveChangesAsync(new CancellationToken()), Times.Once);

        }

        [Fact]
        public async Task Edit_Item()
        {
            //Arrange
            var ctx = MockDbContextHelper.InMemoryDbContext();
            IDbService<Odd> odsService = new OddsDbService(ctx);
            //Act

            var odd = await odsService.GetSingle(1);
            odd.Odd1 = 3;
            odd.Odd2 = 3;
            odd.OddX = 3;

            await odsService.Edit(odd);

            //Assert
            Assert.Equal(odd, ctx.Odds.FirstOrDefault());

        }

        [Fact]
        public async Task Create_Item_MethodCall()
        {
            //Arrange
            var ctx = appDbContext.Object;
            IDbService<Odd> odsService = new OddsDbService(ctx);
            //Act

            var odd = new Odd()
            {
                HomeTeamId = 2,
                GuestTeamId = 3,
                Odd1 = 1,
                Odd2 = 2,
                OddX = 3,
            };
            
            await odsService.Create(odd);

            //Assert
            appDbContext.Verify(c => c.SaveChangesAsync(new CancellationToken()), Times.Once);

        }

        [Fact]
        public async Task Create_Item_OneItem_More()
        {
            //Arrange
            var ctx = MockDbContextHelper.InMemoryDbContext();
            IDbService<Odd> odsService = new OddsDbService(ctx);
            int oddsCount = ctx.Odds.Count();

            //Act
            var odd = new Odd()
            {
                Id = 2,
                HomeTeamId = 2,
                GuestTeamId = 1,
                Odd1 = 1,
                Odd2 = 2,
                OddX = 3,
            };

            await odsService.Create(odd);

            //Assert
            Assert.Equal(oddsCount+1, ctx.Odds.Count());

        }

        [Fact]
        public async Task Publish_Odds_Check_If_Count_Equals()
        {
            //Arrange
            var ctx = MockDbContextHelper.InMemoryDbContext();
            IDbService<Odd> odsService = new OddsDbService(ctx);
            int oddsCount = ctx.Odds.Count();
            
            //Act

            await odsService.Publish();

            //Assert
            Assert.Equal(oddsCount, ctx.PublicOdds.Count());

        }

        [Fact]
        public async Task PublicOdds_GetAll_NotEmpty()
        {
            //Arrange
            var ctx = MockDbContextHelper.InMemoryDbContext();
            IReadDbService<PublicOddsDTO> odsService = new PublicOddsDbService(ctx);
            
            //Act

            var pubOdds = await odsService.GetAll();

            //Assert
            Assert.NotEmpty(pubOdds);

        }
    }
}
