﻿using System;
using System.Collections.Generic;
using System.Text;
using gig_dev_test.Data;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;

namespace gig_dev_test.xUnit.DbContextMock
{
    public class MockDbContextHelper
    {
        public static Mock<ApplicationDbContext> SampleDbContext()
        {
            var appDbContext = new Mock<ApplicationDbContext>();

            var team1 = new Team {Id = 1, TeamName = "EMIRATES TEAM NEW ZEALAND"};
            var team2 = new Team {Id = 2, TeamName = "MALTA ALTUS CHALLENGE"};

            IList <Team> teams = new List<Team>()
            {
                team1,
                team2
            };

            IList<Odd> odds = new List<Odd>()
            {
                new Odd
                {
                    Id = 1,
                    HomeTeamId = 1,
                    GuestTeamId = 2,
                    HomeTeam = team1,
                    GuestTeam = team2,
                    Odd1 = 0.1,
                    OddX = 1,
                    Odd2 = 5
                }
            };
            
            appDbContext.Setup(x => x.Teams).ReturnsDbSet(teams);
            appDbContext.Setup(x => x.Odds).ReturnsDbSet(odds);

            return appDbContext;
        }

        public static ApplicationDbContext InMemoryDbContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var ctx = new ApplicationDbContext(options);

            var team1 = new Team { Id = 1, TeamName = "EMIRATES TEAM NEW ZEALAND" };
            var team2 = new Team { Id = 2, TeamName = "MALTA ALTUS CHALLENGE" };

            ctx.Teams.Add(team1);
            ctx.Teams.Add(team2);

            ctx.Odds.Add(new Odd
            {
                Id = 1,
                HomeTeamId = team1.Id,
                GuestTeamId = team2.Id,
                HomeTeam = team1,
                GuestTeam = team2,
                Odd1 = 0.1,
                OddX = 1,
                Odd2 = 5
            });

            ctx.PublicOdds.Add(new PublicOdd
            {
                Id = 2,
                HomeTeamId = team1.Id,
                GuestTeamId = team2.Id,
                HomeTeam = team1,
                GuestTeam = team2,
                Odd1 = 0.1,
                OddX = 1,
                Odd2 = 5
            });

            ctx.SaveChanges();
            return ctx;
        }

    }
}
