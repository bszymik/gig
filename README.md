Requirements:
.NET Core 2.2
Visual Studio 2017
SQL Server (default is localdb but can be set by connection string)

To set the connection string please edit appsettings.json:
"DefaultConnection": "Server=(localdb)\\mssqllocaldb;Database=gig_dev_test;Trusted_Connection=True;MultipleActiveResultSets=true"

To run the project follow the steps:

1. Open the solution in Visual Studio
2. Make sure that gig_dev_test is a startup project
3. Go to package manager console and update database by command: update-database
4. It will run all the migrations and seed the data
5. Hit Run (IIS Express)


For displaying the current odds just open the startup page
For managing the odds go to Odds management in top menu (you can CRUD the odds and publish as Public Odds - visible on main page)
When button Publish Odds will be pressed than all the Odds will be published and visible on main page (users don't need to refresh the page as it will be notified by signalr about new data) - it can be verified by date on the top or by opening 2 browser windows side by side

Few words about DB:
Odds - table containning all the current Odds - available only for odds handler
Teams - list of teams that can be used while creating the odds
PublicOdds - all the odds that has been published - those are publicly available